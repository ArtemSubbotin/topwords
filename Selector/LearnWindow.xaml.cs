﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selector
{
  /// <summary>
  /// Interaction logic for LearnWindow.xaml
  /// </summary>
  public partial class LearnWindow : Window, IStateMaintainable
  {
    private String _lastStateName;

    public LearnWindow()
    {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      ViewModel.Loaded(this);
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      ViewModel.Deactivated();
    }

    private void btnPrev_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Prev();
    }

    private void btnNext_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Next();
    }

    public void GoToState(string stateName)
    {
      if (_lastStateName == stateName)
        return;

      VisualStateManager.GoToElementState(root, stateName, true);
      _lastStateName = stateName;
    }

    private void btnReset_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Reset();
    }

    private void btnAgain_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Again();
    }

    private void btnEng_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.ShowHint();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Selector
{
  public class LearnViewModel : BaseViewModel
  {
    private List<Word> _allWords;
    private Meta _meta;
    
    private List<Word> _wordsToShow;

    
    private DateTime _now;
    private IStateMaintainable _view;
    
    private Int32 _currentWordIndex;

    private Int32 _wordsCount;
    public Int32 WordsCount
    {
      get { return _wordsCount; }
      set { _wordsCount = value; Notify(nameof(WordsCount)); }
    }

    private Int32 _lastShownWordId;
    public Int32 LastShownWordId
    {
      get { return _lastShownWordId; }
      set { _lastShownWordId = value; Notify(nameof(LastShownWordId)); }
    }


    private Word _currentWord = null;
    public Word CurrentWord
    {
      get { return _currentWord; }
      set {
        _currentWord = value;
        Notify(nameof(CurrentWord));

        if (_currentWord?.Id > LastShownWordId)
          LastShownWordId = _currentWord.Id;
      }
    }

    private List<HintItem> _hintItems = null;
    public List<HintItem> HintItems
    {
      get { return _hintItems; }
      set { _hintItems = value; Notify(nameof(HintItems)); }
    }


    internal void Loaded(IStateMaintainable view)
    {
      _view = view;

      _now = DateTime.Now;

      _meta = Model.LoadMeta();
      _allWords = Model.LoadAllWords().OrderBy(x => x.Id).ToList();

      LastShownWordId = _meta.LastShownWordId ?? 0;
      WordsCount = _allWords.Count;

      Reset();
    }

    public void Reset()
    {
      var lastShownWordId = GetLastShownWordId();

      var allUnknownWords = _allWords.Where(x => x.Id < lastShownWordId && !x.IsLearned).ToList();

      _wordsToShow = Fill(10,
              () => allUnknownWords.Where(x => x.IsTimeShowNow() == true).OrderByDescending(x => x.GetPriority()), //these words have been shown before
              () => _allWords.Where(x => x.Id > lastShownWordId)); //these words to demonstrate first time

      SetCurrentWord(0);
    }

    internal void Next()
    {
      SetCurrentWord(_currentWordIndex + 1);
    }

    internal void Prev()
    {
      SetCurrentWord(_currentWordIndex - 1);
    }

    internal void Again()
    {
      SetCurrentWord(0);
    }

    internal void ShowHint()
    {
      if (CurrentWord.ShownCount == 0 && CurrentWord.LastShownDate == null)
      {
        _view.GoToState("RusState");
        return;
      }

      var hintWords = Random(_allWords, 9);
      var hintItems = hintWords.Select(x => new HintItem(x.RusText)).ToList();
      var correctHintIndex = hintWords.IndexOf(CurrentWord);
      if (correctHintIndex < 0)
        correctHintIndex = R.Next(hintItems.Count);
      hintItems[correctHintIndex] = new HintItem(CurrentWord.RusText, isCorrect: true);

      HintItems = hintItems;

      _view.GoToState("HintState");
    }

    private static Random R = new Random();

    public List<T> Random<T>(List<T> sourceList, Int32 count)
    {
      var res = new List<T>(count);

      while (res.Count < count)
      {
        var w = sourceList[R.Next(sourceList.Count)];

        if (res.Contains(w))
          continue;

        res.Add(w);
      }

      return res;
    }

    private void SetCurrentWord(int index)
    {
      if (CurrentWord != null && CurrentWord.LastShownDate != _now)
      {
        CurrentWord.LastShownDate = _now;
        CurrentWord.ShownCount++;
      }

      if (index < 0)
      {
        index = 0;
      }
      else if (index >= _wordsToShow.Count)
      {
        _view.GoToState("FinishState");
        return;
      }

      _currentWordIndex = index;
      CurrentWord = _wordsToShow[_currentWordIndex];

      _view.GoToState("DefaultState");
    }

    private List<T> Fill<T>(Int32 resCount, params Func<IEnumerable<T>> [] fillFunctions)
    {
      var res = new List<T>();

      for (int i = 0; i < fillFunctions.Length; i++)
      {
        resCount = resCount - res.Count;
        res.AddRange(fillFunctions[i]().Take(resCount));
      }

      return res;
    }

    internal void Deactivated()
    {
      _meta.LastLaunchDate = _now;
      _meta.LastShownWordId = GetLastShownWordId();
      Model.UpdateMeta(_meta);

      Model.UpdateWords(_allWords);
    }

    private int GetLastShownWordId()
    {
      if (_wordsToShow == null)
        return _meta.LastShownWordId ?? 0;

      var tmp = _wordsToShow.Where(x => x.LastShownDate == _now);
      if (tmp.Count() == 0)
        return _meta.LastShownWordId ?? 0;

      return Math.Max(_meta.LastShownWordId ?? 0, tmp.Max(x => x.Id));
    }
  }
}

﻿using System;

namespace Selector
{
  internal interface IStateMaintainable
  {
    void GoToState(String stateName);
  }
}
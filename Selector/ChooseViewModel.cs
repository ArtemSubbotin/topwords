﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selector
{
  public class ChooseViewModel : BaseViewModel
  {
    private List<Word> _words;
    private List<Word> _unknwnWords;

    private Int32 _wordIndex = -1;

    private Word _currentWord;
    public Word CurrentWord
    {
      get { return _currentWord; }
      set { _currentWord = value; Notify(nameof(CurrentWord)); }
    }

    private String _statusText;
    public String StatusText
    {
      get { return _statusText; }
      set { _statusText = value; Notify(nameof(StatusText)); }
    }

    private Boolean _showTranslation = true;
    public Boolean ShowTranslation
    {
      get { return _showTranslation; }
      set { _showTranslation = value; Notify(nameof(ShowTranslation)); }
    }


    internal Boolean KeyPressed(Key key)
    {
      Boolean handled = true;
      if (key == Key.Left)
      {
        SetPrevWord();
      }
      else if (key == Key.Right)
      {
        SetNextWord();
      }
      else if (key == Key.Space)
      {
        Learn();
      }
      else if (key == Key.T || key == Key.Tab)
      {
        ShowTranslation = !ShowTranslation;
      }
      else
      {
        handled = false;
      }

      return handled;
    }

    internal void Learn()
    {
      if (IsCurrentWordUnknown())
      {
        _unknwnWords.Remove(_unknwnWords.First(x => x.Id == CurrentWord.Id));
        ShowTranslation = false;
        StatusText = "known word";
      }
      else
      {
        ShowTranslation = true;
        _unknwnWords.Add(CurrentWord);
        StatusText = "unknown word";
      }
    }

    internal void Init()
    {
      _words = Model.LoadAllWords();
      if (File.Exists("unknown.json"))
        _unknwnWords = Model.LoadUnknownWords();
      else
        _unknwnWords = new List<Word>();

      var savedWordIndex = Model.LoadLastWordIndex();
      if (savedWordIndex > 0)
        _wordIndex = savedWordIndex - 1;

      SetNextWord();
    }

    public void SetNextWord()
    {
      var wordIndex = _wordIndex + 1;
      if (wordIndex >= _words.Count)
        _wordIndex = 0;
      else
        _wordIndex = wordIndex;

      SetWord();
    }

    private void SetWord()
    {
      CurrentWord = _words[_wordIndex];
      StatusText = IsCurrentWordUnknown() ? "unknown word" : "";
      ShowTranslation = IsCurrentWordUnknown();
    }

    private bool IsCurrentWordUnknown()
    {
      return _unknwnWords.Any(x => x.Id == CurrentWord.Id);
    }

    public void SetPrevWord()
    {
      var wordIndex = _wordIndex - 1;
      if (wordIndex < 0)
        _wordIndex = _words.Count - 1;
      else
        _wordIndex = wordIndex;

      SetWord();
    }

    public void Save()
    {
      Model.UpdateUnknownWords(_unknwnWords);
      Model.UpdateLastWordIndex(_wordIndex);
    }
  }
}

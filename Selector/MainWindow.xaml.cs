﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selector
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void Window_KeyUp(object sender, KeyEventArgs e)
    {
      e.Handled = ViewModel.KeyPressed(e.Key);
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      ViewModel.Init();
    }

    private void btnSave_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Save();
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      ViewModel.Save();
    }

    private void btnNext_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.SetNextWord();
    }

    private void btnPrev_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.SetPrevWord();
    }

    private void btnLearn_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Learn();
    }
  }

  
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selector
{
  public class HintItem
  {
    public String HintText { get; set; } = "";

    public Boolean IsCorrect { get; set; } = false;

    public HintItem(String hintText, Boolean isCorrect = false)
    {
      HintText = hintText;
      IsCorrect = isCorrect;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selector
{
  public class MultyChooseViewModel: BaseViewModel
  {
    private static Int32 _count = 9;

    private List<Word> _allWords;

    private Boolean _isAutoPlay = false;
    public Boolean IsAutoPlay
    {
      get { return _isAutoPlay; }
      set { _isAutoPlay = value; Notify(nameof(IsAutoPlay)); }
    }

    private void SetAutoPlay()
    {
      var period = (Int32)TimeSpan.FromSeconds(_count * 1.2).TotalMilliseconds;
      if (_timer == null)
        _timer = new Timer(OnTimerTick, null, period, period);
    }

    private void OnTimerTick(object state)
    {
      if (!IsAutoPlay)
        return;

      Next();
    }

    private Int32 _curWordIndex;
    public Int32 CurWordIndex
    {
      get { return _curWordIndex; }
      set { _curWordIndex = value; Notify(nameof(CurWordIndex)); }
    }

    private Int32 _maxIndex = 100;
    public Int32 MaxIndex
    {
      get { return _maxIndex; }
      set { _maxIndex = value; Notify(nameof(MaxIndex)); }
    }

    private List<WordViewModel> _words;

    private Timer _timer;

    public List<WordViewModel> Words
    {
      get { return _words; }
      set { _words = value; Notify(nameof(Words)); }
    }


    private List<Word> _unknownWords { get; set; }

    internal bool KeyPressed(Key key)
    {
      throw new NotImplementedException();
    }

    internal void Init()
    {
      _allWords = Model.LoadAllWords();
      MaxIndex = _allWords.Count;
      CurWordIndex = Math.Max(0, Model.LoadLastWordIndex());
      _unknownWords = Model.LoadUnknownWords();

      SetWords(CurWordIndex);

      SetAutoPlay();
    }

    public void Next()
    {
      SetWords(CurWordIndex +_count);
    }

    internal void Prev()
    {
      SetWords(CurWordIndex - _count);
    }

    private void SetWords(Int32 index)
    {
      UpdateUnknownWords(Words);

      CurWordIndex = index;
      var res = _allWords.Skip(index).Take(_count).Select(x => new WordViewModel(x)).ToList();

      foreach (var word in res)
      {
        word.IsUnknown = _unknownWords.Any(x => x.Id == word.Word.Id);
      }

      Words = res;
    }

    private void UpdateUnknownWords(List<WordViewModel> wordVMs)
    {
      if (wordVMs?.Count > 0)
      {
        foreach (var wordVM in wordVMs)
        {
          if (wordVM.IsUnknown)
          {
            var item = _unknownWords.FirstOrDefault(x => x.Id == wordVM.Word.Id);
            if (item == null)
              _unknownWords.Add(wordVM.Word);
          }
          else
          {
            var item = _unknownWords.FirstOrDefault(x => x.Id == wordVM.Word.Id);
            if (item != null)
              _unknownWords.Remove(item);
          }
        }
      }
    }

    public void Save()
    {
      UpdateUnknownWords(Words);

      Model.UpdateUnknownWords(_unknownWords);
      Model.UpdateLastWordIndex(CurWordIndex);
    }

    public void Print()
    {
      var top = Model.Print(_unknownWords, true);
      var bottom = Model.Print(_unknownWords, false);
    }
  }

  public class WordViewModel : BaseViewModel
  {
    private Boolean _isUnknown = false;
    public Boolean IsUnknown
    {
      get { return _isUnknown; }
      set { _isUnknown = value; Notify(nameof(IsUnknown)); }
    }

    private Word _word;
    public Word Word
    {
      get { return _word; }
      set { _word = value; Notify(nameof(Word)); }
    }


    public WordViewModel(Word word)
    {
      _word = word;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selector
{
  [DebuggerDisplay("{Id}: {EngText}-{RusText} (IsLearned={IsLearned})")]
  public class Word
  {
    private static Dictionary<Int32, Int32> _repeatIntervalMap = new Dictionary<int, int>()
    {
      { 1, 1},
      { 2, 1},
      { 3, 2},
      { 4, 3},
      { 5, 5},
      { 6, 8},
      { 7, 13},
      { 8, 21},
      { 9, 34},
      { 10, 55}
    };

    public Int32 Id { get; set; }
    public String Type { get; set; }
    public String EngText { get; set; }
    public String RusText { get; set; }

    public Int32 ShownCount { get; set; }
    public DateTime? LastShownDate { get; set; }
    public Boolean IsLearned { get; set; } = false;

    internal Boolean? IsTimeShowNow()
    {
      if (LastShownDate == null || ShownCount == 0)
        return null;

      if (IsLearned)
        return false;

      if (ShownCount > 10)
        return false; //it's learned actually

      var noShowDaysCount = (Int32)((DateTime.Now - LastShownDate.Value).TotalDays);
      return noShowDaysCount >= _repeatIntervalMap[ShownCount];
    }

    internal Int32 GetPriority()
    {
      if (LastShownDate == null || ShownCount == 0)
        return Int32.MaxValue;

      if (IsLearned)
        return Int32.MaxValue;

      return Int32.MaxValue - ShownCount; //the less the word has been shown the more priority it has
    }
  }

  public class Meta
  {
    public Int32? LastShownWordId { get; set; } = 0;
    public DateTime? LastLaunchDate { get; set; }
  }
}

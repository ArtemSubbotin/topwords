﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selector
{
  /// <summary>
  /// Interaction logic for MultyChooseWindow.xaml
  /// </summary>
  public partial class MultyChooseWindow : Window
  {
    public MultyChooseWindow()
    {
      InitializeComponent();
    }


    private void Window_KeyUp(object sender, KeyEventArgs e)
    {
      e.Handled = ViewModel.KeyPressed(e.Key);
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      ViewModel.Init();
    }

    private void btnNext_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Next();
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      ViewModel.Save();
    }

    private void btnPrev_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Prev();
    }

    private void btnPrint_Click(object sender, RoutedEventArgs e)
    {
      ViewModel.Print();
    }
  }
}

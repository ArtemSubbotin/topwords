﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selector
{
  public class Model
  {
    internal static List<Word> LoadAllWords()
    {
      return DeserializeFromFile<List<Word>>("words", new List<Word>());
    }

    public static void UpdateMeta(Meta meta)
    {
      SerializeToFile("meta", meta);
    }

    public static void UpdateWords(List<Word> words)
    {
      SerializeToFile("words", words);
    }

    internal static Meta LoadMeta()
    {
      return DeserializeFromFile<Meta>("meta", new Meta());
    }

    public static void SerializeToFile<T>(String fileId,T objectToSave)
    {
      File.WriteAllText(GetFileName(fileId), JsonConvert.SerializeObject(objectToSave));
    }

    public static T DeserializeFromFile<T>(String fileId, T defaultValue)
    {
      if (!File.Exists(GetFileName(fileId)))
        return defaultValue;

      return JsonConvert.DeserializeObject<T>(File.ReadAllText(GetFileName(fileId)));
    }

    private static string GetFileName(string fileId)
    {
      return $@"{fileId}.json";
    }

    #region print as html
    internal static String Print(List<Word> words, Boolean isTopSide)
    {
      if (isTopSide)
        return Print(words.OrderBy(x => x.Id).Select(x => x.EngText).ToList(), true);
      else
        return Print(words.OrderBy(x => x.Id).Select(x => x.RusText).ToList(), false);
    }

    private static String Print(List<String> words, Boolean fromLeftToRight)
    {
      var colCount = 4;
      var rowCunt = 10;

      var pageChunks = ChunkBy(words, colCount * rowCunt);
      var res = "";

      foreach (var pageChunk in pageChunks)
      {
        var lineChunks = ChunkBy(pageChunk, colCount);

        var items = "";
        foreach (var lineChunk in lineChunks)
        {
          if (!fromLeftToRight)
            lineChunk.Reverse();

          items += string.Join(Environment.NewLine, lineChunk.Select(x => String.Format("<div class=\"grid-item\">{0}</div>", x)));
        }

        res += String.Format("<div class=\"grid-container\">{0}</div>{1}<div class=\"pagebreak\"/>{1}", items, Environment.NewLine);
      }

      return res;
    }

    internal static List<Word> LoadUnknownWords()
    {
      throw new NotImplementedException();
    }

    internal static Int32 LoadLastWordIndex()
    {
      throw new NotImplementedException();
    }

    public static List<List<T>> ChunkBy<T>(List<T> source, int chunkSize)
    {
      return source
          .Select((x, i) => new { Index = i, Value = x })
          .GroupBy(x => x.Index / chunkSize)
          .Select(x => x.Select(v => v.Value).ToList())
          .ToList();
    }

    internal static void UpdateLastWordIndex(int curWordIndex)
    {
      throw new NotImplementedException();
    }

    internal static void UpdateUnknownWords(List<Word> _unknownWords)
    {
      throw new NotImplementedException();
    }
    #endregion
  }
}

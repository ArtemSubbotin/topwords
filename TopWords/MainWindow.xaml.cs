﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace TopWords
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private async void btnStart_Click(object sender, RoutedEventArgs e)
    {
      //var b = JsonConvert.DeserializeObject<List<Word>>(File.ReadAllText("words.json"));


      var sw = Stopwatch.StartNew();
      var text = File.ReadAllText("words.xml");
      var words = new List<Word>();
      var matches = Regex.Matches(text, "id=\"(\\d+)\"\\stype=\"(\\w)\">(\\w+)<");

      foreach (Match match in matches)
      {
          var w = new Word()
          {
            Id = Int32.Parse(match.Groups[1].Value),
            Type = match.Groups[2].Value,
            EngText = match.Groups[3].Value
          };
          var translated = await w.UpdateTranslation();
          if (translated)
            words.Add(w);

          Debug.WriteLine($@"{w.Id} {w.EngText}-{w.RusText}");
      }
      var serializedWords = JsonConvert.SerializeObject(words);
      File.WriteAllText("words.json", serializedWords);

      sw.Stop();
      
      Application.Current.Dispatcher.Invoke(new Action(() => 
      {
        MessageBox.Show($"elapsed miutes: {sw.Elapsed.TotalMinutes}, words: {words.Count}");
      }));
    }

    
  }

  [DebuggerDisplay("{Id}: {EngText}-{RusText}")]
  public class Word
  {
    public Int32 Id;
    public String Type;
    public String EngText;
    public String RusText;

    public async Task<Boolean> UpdateTranslation()
    {
      if (String.IsNullOrEmpty(EngText))
        return false;

      var index = 0;
      while (index < 3)
      {
        index++;
        try
        {
          var translations = await YaTranslator.Translate(EngText);
          RusText = String.Join(",", translations);
          return true;
        }
        catch { }
      }

      return false;
    }
  }
  
}

﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopWords
{
  public static class YaTranslator
  {
    private static String API_KEY = "trnsl.1.1.20170831T125038Z.917e9400d6c45b81.614ecf1d69f35e94d58f8363772c527189b49860";

    public static async Task<List<String>> Translate(String text, String lang = "en-ru")
    {
      var format = "plain";
      var res = await $@"https://translate.yandex.net/api/v1.5/tr.json/translate?key={API_KEY}&text={text}&lang={lang}&format={format}"
        .GetAsync()
        .ReceiveJson();
      return (res.text as List<Object>).OfType<String>().ToList();
    }
  }
}

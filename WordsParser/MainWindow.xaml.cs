﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordsParser
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void btnParse_Click(object sender, RoutedEventArgs e)
    {
      var lines = File.ReadAllLines("words.txt");
      var pattern = new Regex(@"(\d+)\s+(\w+)\s+(\w+)");

      var res = new StringBuilder();
      res.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
      res.AppendLine("<words>");

      var id = 1;
      foreach (var line in lines)
      {
        var match = pattern.Match(line);
        var word = match.Groups[2];
        var type = match.Groups[3];
        res.AppendLine($"<word id=\"{id++}\" type=\"{type}\">{word}</word>");
      }
      
      res.AppendLine("</words>");
      File.WriteAllText("res.xml", res.ToString());
    }
  }
}
